---
title: "Wystartowaliśmy!"
date: 2021-01-22T21:06:37+01:00
draft: false
---

Chcieliśmy stworzyć stronę na  której w jednym miejscu można znaleźć
informacje, dokumentacje i porady w języku polskim o naszym ulubionym edytorze
tekstu jakim jest vim/neovim.

Pragniemy aby nasi czytelnicy zaangażowali się w tworzenie witryny, możesz
dodawać treści, linki do ciekawych artykułów w sieci, czy zaangażować się
w tłumaczenie dokumentacji.

Jeżeli znalazłeś błąd na stronie możesz założyć
[`issues`](https://gitlab.com/marcin-klejna/vim.org.pl/-/issues) w naszym
repozytorium.

Witryna jest we wczesnym stadium rozwoju więc wszelkie uwagi i propozycje są
mile widziane.
