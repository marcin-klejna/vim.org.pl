---
title: "Polskie wiki edytora vim i neovim"
linkTitle: "Polskie wiki edytora vim i neovim"
---

{{< blocks/cover title="Witaj na wyspie vim. Możesz współtworzyć projekt!" image_anchor="top" height="full" color="transparent" >}}
<div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref "/docs/vim-podrecznik-uzytkownika/" >}}">
		Podręcznik użytkownika <i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
	<a class="btn btn-lg btn-secondary mr-3 mb-4" href="https://github.com/neovim/neovim/wiki/Installing-Neovim">
		Pobierz <i class="fab fa-github ml-2 "></i>
	</a>
</div>
{{< /blocks/cover >}}

