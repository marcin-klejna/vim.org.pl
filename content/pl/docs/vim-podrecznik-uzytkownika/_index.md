---
title: "Vim Podręcznik Użytkownika"

hide_feedback: true
---

Polskie [tłumaczenie]({{< ref "usr_01#tlumacz" >}}) mamy dzięki pracy Mikołaja Machowskiego. Można pobrać te pliki w formacie pomocy vima https://github.com/vvizzo/vim-doc-pl.
