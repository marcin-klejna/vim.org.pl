[![Netlify Status](https://api.netlify.com/api/v1/badges/25e53e06-ab15-474e-b545-b95769981d69/deploy-status)](https://app.netlify.com/sites/neovim/deploys)

# Witaj

To repozytorium zawiera zasoby wymagane do zbudowania witryny [vim.org.pl](https://vim.org.pl/). Cieszymy się, że chcesz wnieść swój wkład!

## Korzystanie z tego repozytorium

Możesz uruchomić witrynę lokalnie za pomocą [Hugo](https://gohugo.io/). Nasza witryna kożysta z szablonu [Doscy](https://github.com/google/docsy#readme)

## Twój wkład w dokumentację

Możesz kliknąć przycisk Fork w prawym górnym rogu ekranu, aby utworzyć kopię tego repozytorium na swoim koncie GitLab. Wprowadź dowolne zmiany w swoim forku, a kiedy będziesz gotowy, aby wysłać te zmiany do nas, przejdź do swojej kopii i stwórz nowy `Merge Requests`, aby nas o tym poinformować

Możesz tesz składać propozycjie i zmiany poprzez tworzenie [issues](https://gitlab.com/marcin-klejna/vim.org.pl/-/issues).

## Społeczność

Zajrzyj na stronę [społeczności](link), aby dowiedzieć się, jak możesz zaangażować się w jej działania.

## Szablony stron
(Do zrobienia...)
